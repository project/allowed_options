## Introduction


The Allowed Options module provides the ability to select allowed options for
  `list_*` field types in "Manage form display" settings, hence it allows sharing
  the same field storage across multiple bundles but with different options.

Currently it supports the following field types:
 - List (float);
 - List (integer);
 - List (string);

For a full description of the module, visit the project page:
   https://www.drupal.org/project/allowed_options

To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/allowed_options


## Requirements


This module requires no modules outside of Drupal core. It is compatible with
  Drupal 8 and Drupal 9 versions.


## Installation


Install as you would normally install a contributed Drupal module. Visit
  https://www.drupal.org/node/1897420 for further information.


## Configuration


 * Go to "Manage form display" page of the bundle;

 * Open widget settings of one of the `list_*` fields;

 * Select allowed options and save your changes.
 

## Maintainers

- Ivan Doroshenko (Matroskeen) - https://www.drupal.org/u/matroskeen

